const express = require('express');
const bodyParser = require('body-parser');
const { getFilledSeats } = require('./layout');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/api/layout', (req, res, next) => {
    console.log(req.body);
    try {
        console.log(req.body);
        const layout = JSON.parse(req.body.layout);
        const numberOfPeople = +req.body.numberOfPassengers;
        const seatingArrangement = getFilledSeats(layout, numberOfPeople);
        res.json(seatingArrangement);
    } catch (e) {
        next(e);
    }
});

app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).send({ error: err.message });
});


app.listen(port, () => {
    console.log(`Server running on ${port}`);
});