const fillSeats = (maxRows, blocks, cohort, structure, seatType) => {
    for(var rowIndex = 0; rowIndex < maxRows; rowIndex++) {
        for (var blockIndex = 0; blockIndex < blocks.length; blockIndex++) {
            for (var columnIndex = 0; columnIndex < blocks[blockIndex][0]; columnIndex++) {
                if (cohort.currentPassengerIndex <= cohort.numberOfPeople) {
                    if (structure[blockIndex][rowIndex] != undefined && structure[blockIndex][rowIndex][columnIndex].seatType == seatType) {
                        structure[blockIndex][rowIndex][columnIndex].passengerNumber = cohort.currentPassengerIndex;
                        cohort.currentPassengerIndex++;
                    }
                }
            }
        }
    }
};

const getFilledSeats = (blocks, numberOfPeople) => {
    console.log("Number Of People");
    console.log(numberOfPeople);
    var currentPassengerIndex = 1;
    
    var maxRows = blocks.reduce((maxRow, block) => {
        maxRow = Math.max(maxRow, block[1]); 
        return maxRow;
    }, 0);
    
    var structure = blocks.reduce((structure, block, blockIndex) => {
        structure[blockIndex] = [];
        for (var rowIndex = 0; rowIndex < block[1]; rowIndex++) {
            structure[blockIndex][rowIndex] = [];
            for(var columnIndex = 0; columnIndex < block[0]; columnIndex++) {
                structure[blockIndex][rowIndex][columnIndex] = {};
                if ((blockIndex == 0 && columnIndex == 0) || (blockIndex == blocks.length - 1 && columnIndex == blocks[blockIndex][0] - 1)) {
                    structure[blockIndex][rowIndex][columnIndex].seatType = "window";
                } else if ((columnIndex == block[0] - 1) || columnIndex == 0) {
                    structure[blockIndex][rowIndex][columnIndex].seatType = "aisle";
                } else if (columnIndex < block[0] - 1) {
                    structure[blockIndex][rowIndex][columnIndex].seatType = "middle";
                }
            }
        }
        return structure;      
    }, []);

    const cohort = { currentPassengerIndex, numberOfPeople };
    fillSeats(maxRows, blocks, cohort, structure, "aisle");
    fillSeats(maxRows, blocks, cohort, structure, "window");
    fillSeats(maxRows, blocks, cohort, structure, "middle");

    return structure;
};

module.exports = { getFilledSeats };